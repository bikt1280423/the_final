﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class EnemyHit : MonoBehaviour 
{
    public Text countText;
      private int count;
    void Start()
    {
        count = 0;
        SetCountText();
       
    }
 
    //オブジェクトと接触した瞬間に呼び出される
    void OnTriggerEnter(Collider other)
    {
 
        //攻撃した相手がEnemyの場合
        if(other.CompareTag("Enemy")){
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            count = count + 1;
            SetCountText();
 
        }
    }

    void SetCountText()
    {
          countText.text = "K.O. " + count.ToString ();
    }
}