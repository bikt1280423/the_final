﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Storking : MonoBehaviour
{
    
   // [SerializeField] private Transform destinationTransform;
    private Transform playerTransform;
    private NavMeshAgent navMeshAgent;
    // Start is called before the first frame update
    void Start()
    {
           navMeshAgent = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
            playerTransform = GameObject.Find("Player").transform; 
            navMeshAgent.SetDestination(playerTransform.position);
    }
}
